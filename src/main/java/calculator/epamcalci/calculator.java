package calculator.epamcalci;

import java.util.Scanner;


public class calculator {
	public int add(int i, int j) {
        return i + j;
    }
public int mul(int i, int j) {
        return i * j;
    }
public int sub(int i, int j) {
        return i - j;
    }
public double div(double i, double j) {
        if (j != 0) {
            return i / j;
        } else {
            return 0;
        }
    }

public static void main(String[] args) {
    calculator cal = new calculator();
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter two numbers: ");
    int a, b;
    a = sc.nextInt();
    b = sc.nextInt();
    cal.add(a, b);
    cal.mul(a, b);
    cal.sub(a, b);
    cal.div(a, b);
    sc.close();
}

}
